// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "zk-merkle-tree/contracts/ZKTree.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract ZKTreeVote is ZKTree, Ownable {
    modifier onlyJudge() {
        require(judges[msg.sender]);
        _;
    }

    event JudgeAppointed(address judge);
    event JudgeDissmissed(address judge);
    event EventCreated(GenericEvent);
    event EventDeleted(GenericEvent);
    event ScoringDeadlineChanged(uint256 scoringDeadline);

    struct GenericEvent {
        uint256 id;
        uint[] scores;
        uint scored; // timestamp of first score
    }

    mapping(uint256 => bool) uniqueHashes;
    mapping(address => bool) public judges;
    mapping(uint256 => GenericEvent) public events;
    uint256 public scoringDeadline = 5; // minutes

    constructor(
        uint32 _levels,
        IHasher _hasher,
        IVerifier _verifier
    ) ZKTree(_levels, _hasher, _verifier) {}

    function registerCommitment(
        uint256 _uniqueHash,
        uint256 _commitment
    ) external onlyJudge {
        require(
            !uniqueHashes[_uniqueHash],
            "This unique hash is already used!"
        );
        _commit(bytes32(_commitment));
        uniqueHashes[_uniqueHash] = true;
    }

    function score(
        uint _score,
        uint256 _eventId,
        uint256 _nullifier,
        uint256 _root,
        uint[2] memory _proof_a,
        uint[2][2] memory _proof_b,
        uint[2] memory _proof_c
    ) external {
        require(_score <= 10, "Invalid score");
        require(events[_eventId].id > 0, "Event with this id does not exist");
        require(
            events[_eventId].scored == 0 ||
                block.timestamp <=
                events[_eventId].scored + scoringDeadline * 1 minutes,
            "Deadline is reached"
        );

        _nullify(
            bytes32(_nullifier),
            bytes32(_root),
            _proof_a,
            _proof_b,
            _proof_c
        );

        events[_eventId].scores.push(_score);

        if (events[_eventId].scored == 0) {
            events[_eventId].scored = block.timestamp;
        }
    }

    function getAverageScore(uint _eventId) external view returns (uint) {
        require(events[_eventId].id > 0, "Event with this id does not exist");
        require(events[_eventId].scores.length > 0, "Event is not scored");
        require(
            block.timestamp >
                events[_eventId].scored + scoringDeadline * 1 minutes,
            "Deadline is not reached"
        );

        uint total = 0;
        for (uint256 i = 0; i < events[_eventId].scores.length; i++) {
            total += events[_eventId].scores[i];
        }

        return total / events[_eventId].scores.length;
    }

    function setScoringDeadline(uint256 _scoringDeadline) external onlyOwner {
        require(
            _scoringDeadline > 0 && _scoringDeadline <= 60 * 24,
            "Invalid deadline"
        );

        scoringDeadline = _scoringDeadline;

        emit ScoringDeadlineChanged(_scoringDeadline);
    }

    function createEvent(GenericEvent calldata _event) external onlyOwner {
        require(_event.id > 0, "Event id must be greater than 0");
        require(events[_event.id].id == 0, "Event with this id already exists");

        events[_event.id] = _event;

        emit EventCreated(events[_event.id]);
    }

    function deleteEvent(uint256 _id) external onlyOwner {
        require(_id > 0, "Event id must be greater than 0");
        require(events[_id].id > 0, "Event with this id does not exist");

        emit EventDeleted(events[_id]);

        delete events[_id];
    }

    function appointJudge(address _judge) external onlyOwner {
        require(_judge != address(0), "Invalid address");
        require(!judges[_judge], "Judge already appointed");

        judges[_judge] = true;

        emit JudgeAppointed(_judge);
    }

    function dismissJudge(address _judge) external onlyOwner {
        require(_judge != address(0), "Invalid address");
        require(judges[_judge], "Judge is not appointed");

        delete judges[_judge];

        emit JudgeDissmissed(_judge);
    }
}
